# build the image
docker build -t docker-go-test .
You can check the Docker Desktop -> Images, or VSCode extension images

#To start your application in the container
docker run --rm -p 8001:8000 -v $(pwd):/app -d docker-go-test
``
-p 8001:8000 - This exposes our application which is running on port 8000 within our container on http://localhost:8001 on our local machine.
-it - This flag specifies that we want to run this image in interactive mode with a tty for this container process. Replace it with -d for detached mode.
my-go-app - This is the name of the image that we want to run in a container.
-v $(pwd):/app - This copies what you have in your local machine to docker, so code change will get copied over to the container, which will trigger a rebuild by Air
``
# References
https://leogoesger.io/go-and-docker-with-auto-reload/